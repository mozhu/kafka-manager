在0.10.0.0以上的版本中，服务端log增加了timestamp字段
而客户端在0.10.1.0后提供了根据时间搜索offset的方法
kafkaConsumer.offsetsForTimes

如果用0.10.1.0的客户端访问0.10.0.0版本的服务端时，会报错
Cannot create a v0 ListOffsetRequest because we require features supported only in 1 or later.

所以在不支持的版本中，我们要放弃这个功能，只要能显示出offset就可以，对于其时间，暂时不考虑
https://github.com/dpkp/kafka-python/pull/1199

timestamp 添加的原始需求
https://cwiki.apache.org/confluence/display/KAFKA/KIP-32+-+Add+timestamps+to+Kafka+message
