package kafka.admin;

import java.util.List;

public interface ConsumerService {

    List<String> listConsumerGroups();
}
