package kafka.admin;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.kafka.common.Node;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PartitionAssignmentState {

    private String group;
    private Node coordinator;
    private String topic;
    private Long partition;
    private Long offset;
    private Long lag;
    private String consumerId;
    private String host;
    private String clientId;
    private Long logEndOffset;
}
