package com.derbysoft.nuke.kafka.manager.domain.model;

import lombok.Data;

import java.util.List;

@Data
public class TopicPartition {

    private int partition;
    private Node leader;
    private List<Node> replicas;
    private List<Node> isr;

}
