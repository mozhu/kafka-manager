package com.derbysoft.nuke.kafka.manager.dto;

import com.derbysoft.nuke.kafka.manager.domain.model.Broker;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BrokerMetrics {
    private Broker broker;
    private Map<String, Object> metrics;
}
