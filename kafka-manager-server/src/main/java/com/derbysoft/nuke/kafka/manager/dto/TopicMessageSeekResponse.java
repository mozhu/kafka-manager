package com.derbysoft.nuke.kafka.manager.dto;

import com.derbysoft.nuke.kafka.manager.infrastructure.kafka.model.Message;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TopicMessageSeekResponse {
    List<Message<String, String>> messages;
    Map<Integer, Long> lastOffsets;
}
