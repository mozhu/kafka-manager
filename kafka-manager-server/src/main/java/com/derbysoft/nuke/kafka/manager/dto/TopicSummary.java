package com.derbysoft.nuke.kafka.manager.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TopicSummary {
    private String topicName;
    private Integer partitions;
    private Integer replication;
    private Integer totalOffsets;
    private Integer brokers;
    private Integer preferredReplicas;
    private Integer skewedBrokers;
    private Integer spreadBrokers;
    private Integer underReplicatedBrokers;
}
