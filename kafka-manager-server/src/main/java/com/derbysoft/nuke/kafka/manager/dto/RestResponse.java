package com.derbysoft.nuke.kafka.manager.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RestResponse implements Serializable {

    private static final String OPERATION_FAIL = "operation fail";
    private static final String DEFAULT_ERROR_CODE = "-1";
    private static final String DEFAULT_SUCCESS_CODE = "0";
    private String message;
    private Object data;
    private String code = DEFAULT_SUCCESS_CODE;

    public RestResponse() {
    }

    public RestResponse(String code, String message) {
        this(code, message, null);
    }

    public RestResponse(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    private static String defaultFailMessage() {
        return OPERATION_FAIL;
    }

    public static RestResponse fail() {
        return fail(defaultFailMessage());
    }

    public static RestResponse fail(String message) {
        return new RestResponse(DEFAULT_ERROR_CODE, message);
    }

    public static RestResponse fail(String errorCode, String message) {
        return new RestResponse(errorCode, message, null);
    }

    public static RestResponse success() {
        return success(null);
    }

    public static RestResponse success(Object data) {
        return new RestResponse(DEFAULT_SUCCESS_CODE, null, data);
    }

}
