package com.derbysoft.nuke.kafka.manager.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TopicMessageSeekRequest {
    Integer recordsPerPartition;
    List<PartitionSeekInfo> partitionOffsets;
}
