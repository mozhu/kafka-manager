package com.derbysoft.nuke.kafka.manager.domain.model;

import lombok.Data;

@Data
public class Node {
    private int id;
    private String host;
    private int port;
    private String rack;
}
