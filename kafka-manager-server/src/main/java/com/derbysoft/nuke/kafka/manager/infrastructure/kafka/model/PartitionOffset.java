package com.derbysoft.nuke.kafka.manager.infrastructure.kafka.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PartitionOffset {
    private long start;
    private long end;
}
