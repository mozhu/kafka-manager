package com.derbysoft.nuke.kafka.manager.infrastructure.kafka.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.kafka.common.TopicPartition;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConsumerSummary {

    private List<TopicPartition> topicPartitions;
    private String clientId;
    private String consumerId;
    private String host;
}
