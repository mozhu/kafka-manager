package com.derbysoft.nuke.kafka.manager;

import com.derbysoft.nuke.kafka.manager.domain.model.*;
import com.derbysoft.nuke.kafka.manager.dto.*;

import java.util.List;

public interface KafkaManagerService {

    Topic getTopic(Cluster cluster, String topicName);

    List<Topic> getTopics(Cluster cluster);

    TopicSummary getTopicSummary(Cluster cluster, String topicName);

    List<TopicPartitionSummary> getTopicPartitionSummaries(Cluster cluster, String topicName);

    TopicMessageSeekResponse seekMessages(Cluster cluster, String topicName, TopicMessageSeekRequest topicMessageSeekRequest);

    List<TopicSummary> listTopicSummaries(Cluster cluster);

    List<Broker> getBrokers(Cluster cluster);

    List<BrokerMetrics> getBrokersMetrics(Cluster cluster);
}
