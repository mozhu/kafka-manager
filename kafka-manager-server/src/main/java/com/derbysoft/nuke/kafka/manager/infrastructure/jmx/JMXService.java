package com.derbysoft.nuke.kafka.manager.infrastructure.jmx;

import com.derbysoft.nuke.kafka.manager.domain.model.Broker;

import java.util.Map;

public interface JMXService {

    Map<String, Object> getBrokerTopicMetrics(Broker broker);

    Map<String, Object> getSocketServerMetrics(Broker broker);

    Map<String, Object> getOSMetrics(Broker broker);
}
