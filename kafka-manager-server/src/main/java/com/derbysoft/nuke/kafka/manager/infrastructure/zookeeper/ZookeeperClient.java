package com.derbysoft.nuke.kafka.manager.infrastructure.zookeeper;

import org.apache.curator.framework.CuratorFramework;

public interface ZookeeperClient {

    CuratorFramework getZookeeperClient(String connectString);
}
