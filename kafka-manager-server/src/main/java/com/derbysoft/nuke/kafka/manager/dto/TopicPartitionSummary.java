package com.derbysoft.nuke.kafka.manager.dto;

import com.derbysoft.nuke.kafka.manager.domain.model.TopicPartition;
import com.derbysoft.nuke.kafka.manager.infrastructure.kafka.model.OffsetTimestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TopicPartitionSummary {

    private TopicPartition topicPartition;

    private OffsetTimestamp start;

    private OffsetTimestamp end;

    private long messageSize;

    private boolean underReplicated;

    public boolean isPreferredLeader() {
        return topicPartition.getLeader().equals(topicPartition.getReplicas().get(0));
    }

    public long getMessageSize() {
        return end.getOffset() - start.getOffset();
    }

}
