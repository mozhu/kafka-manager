package com.derbysoft.nuke.kafka.manager.domain.service;

import com.derbysoft.nuke.kafka.manager.domain.model.Cluster;

import java.util.List;

/**
 * Created by Albert Fu on 12/06/2018.
 */
public interface ClusterService {

    Cluster getClusterByCode(String code);

    List<Cluster> getClusters();

    void saveCluster(Cluster cluster);

    void deleteCluster(String code);

    void deleteClusters(String[] codes);

}
