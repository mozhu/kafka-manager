package com.derbysoft.nuke.kafka.manager.infrastructure.zookeeper.data;

import com.derbysoft.nuke.kafka.manager.domain.model.Broker;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * <pre>
 * /brokers/ids
 * [1,2,3]
 * /brokers/ids/[0...N]
 * 每个broker的配置文件中都需要指定一个数字类型的id(全局不可重复),此节点为临时znode(EPHEMERAL)
 *
 * Schema:
 * {
 * "jmx_port": jmx端口号,
 * "timestamp": kafka broker初始启动时的时间戳,
 * "host": 主机名或ip地址,
 * "version": 版本编号默认为1,
 * "port": kafka broker的服务端端口号,由server.properties中参数port确定
 * }
 *
 * Example:
 * {
 * "jmx_port": -1,
 * "timestamp":"1525741823119"
 * "version": 1,
 * "host": "hadoop1",
 * "port": 9092
 * }
 * </pre>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BrokerInfo {
    private String id;
    private String host;
    private Integer port;
    private Integer jmx_port;
    private List<String> endpoints;
    private String timestamp;
    private String version;

    public Broker toBroker() {
        return Broker.builder()
                .id(this.getId())
                .host(this.getHost())
                .port(String.valueOf(this.getPort()))
                .jmxPort(String.valueOf(this.getJmx_port()))
                .endpoints(this.getEndpoints())
                .build();
    }
}
