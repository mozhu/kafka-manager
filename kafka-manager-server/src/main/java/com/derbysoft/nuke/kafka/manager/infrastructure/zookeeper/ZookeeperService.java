package com.derbysoft.nuke.kafka.manager.infrastructure.zookeeper;

import com.derbysoft.nuke.kafka.manager.infrastructure.zookeeper.data.*;

import java.util.List;

public interface ZookeeperService {

    Controller getController();

    List<String> getBrokerIds();

    BrokerInfo getBroker(String brokerId);

    List<BrokerInfo> getBrokers();

    List<String> getTopicNames();

    TopicInfo getTopic(String topic);

    List<String> getPartitions(String topic);

    TopicPartitionState getTopicPartitionState(String topic, String partition);

    List<String> getConsumerGroups();

    List<String> getConsumerIds(String consumerGroup);

    ConsumerInfo getConsumer(String consumerGroup, String consumerId);

    List<ConsumerInfo> getConsumers(String consumerGroup);

}
