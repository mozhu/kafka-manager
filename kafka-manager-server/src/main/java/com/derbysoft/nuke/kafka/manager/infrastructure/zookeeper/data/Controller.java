package com.derbysoft.nuke.kafka.manager.infrastructure.zookeeper.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Controller {
    private String controller_epoch;
    private String brokerid;
    private String timestamp;
    private String version;
}
