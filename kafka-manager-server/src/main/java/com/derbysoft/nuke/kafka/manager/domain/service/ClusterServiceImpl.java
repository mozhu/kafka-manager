package com.derbysoft.nuke.kafka.manager.domain.service;

import com.derbysoft.nuke.kafka.manager.domain.model.Cluster;
import com.derbysoft.nuke.kafka.manager.domain.repository.ClusterRepository;
import com.derbysoft.nuke.kafka.manager.infrastructure.jmx.JMXService;
import com.derbysoft.nuke.kafka.manager.infrastructure.kafka.KafkaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Albert Fu on 12/06/2018.
 */
@Service
public class ClusterServiceImpl implements ClusterService {

    private KafkaClient kafkaClient;

    private JMXService jmxService;

    private ClusterRepository clusterRepository;

    @Override
    public Cluster getClusterByCode(String code) {
        return clusterRepository.getClusterByCode(code);
    }

    @Override
    public List<Cluster> getClusters() {
        return clusterRepository.getClusters();
    }

    @Override
    public void saveCluster(Cluster cluster) {
        clusterRepository.saveCluster(cluster);
    }

    @Override
    public void deleteCluster(String code) {
        clusterRepository.deleteCluster(code);
    }

    @Override
    public void deleteClusters(String[] codes) {
        clusterRepository.deleteClusters(codes);
    }

    @Autowired
    public void setKafkaClient(KafkaClient kafkaClient) {
        this.kafkaClient = kafkaClient;
    }

    @Autowired
    public void setJmxService(JMXService jmxService) {
        this.jmxService = jmxService;
    }

    @Autowired
    public void setClusterRepository(ClusterRepository clusterRepository) {
        this.clusterRepository = clusterRepository;
    }
}
