package com.derbysoft.nuke.kafka.manager.infrastructure.kafka;

import org.apache.kafka.clients.admin.AdminClient;

/**
 * Created by Albert Fu on 18/05/2018.
 */

public interface KafkaClient {

    AdminClient getAdminClient(String bootstrapServers);

    KafkaAdminClient getKafkaAdminClient(String bootstrapServers);
}
