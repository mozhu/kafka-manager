package com.derbysoft.nuke.kafka.manager.infrastructure.zookeeper;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;

public class ZKNodeData {

    private byte[] data;

    private Map<String, Object> map;

    public ZKNodeData(byte[] data) {
        this.data = data;
        try {
            map = (Map<String, Object>) new ObjectMapper().readValue(new String(data), Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getString(String key) {
        Object o = map.get(key);
        return o == null ? null : o.toString();
    }

    public String getStringOrDefault(String key) {
        Object o = map.get(key);
        return o == null ? null : o.toString();
    }

    public <T> T get(String key) {
        return (T) map.get(key);
    }

    public <T> T getOrDefault(String key, T defaultValue) {
        return (T) map.getOrDefault(key, defaultValue);
    }

}
