package com.derbysoft.nuke.kafka.manager.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Cluster {
    private String code;
    private String name;

    /**
     * zk1:2181,zk2:2181,zk3:2181/NAMESPACE
     */
    private String zookeeperServers;

    private Boolean jmxEnable;

    private String jmxUsername;
    private String jmxPassword;

}
