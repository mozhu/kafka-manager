package com.derbysoft.nuke.kafka.manager;

import com.derbysoft.nuke.common.module.config.spring.EnableConfig;
import com.derbysoft.nuke.common.module.rpc.server.autoconfigure.EnableJsonRpc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableConfig(applicationKey = "nuke.kafka.manager")
@EnableJsonRpc(rpcConfig = "classpath:rpc.properties")
@EnableScheduling
public class KafkaManagerApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(KafkaManagerApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(KafkaManagerApplication.class);
    }

}
