package com.derbysoft.nuke.kafka.manager.infrastructure.kafka.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Message<K, V> {

    private String topic;
    private int partition;
    private long offset;
    private long timestamp;

    private K key;
    private V value;
}
