package com.derbysoft.nuke.kafka.manager.infrastructure.kafka.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.kafka.common.Node;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConsumerGroupSummary {

    private Node coordinator;
    private List<ConsumerSummary> consumerSummaries;
    private String assignmentStrategy;

}
