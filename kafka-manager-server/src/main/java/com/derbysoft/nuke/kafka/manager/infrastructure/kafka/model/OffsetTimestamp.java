package com.derbysoft.nuke.kafka.manager.infrastructure.kafka.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OffsetTimestamp {

    private long timestamp;
    private long offset;

    public static OffsetTimestamp empty() {
        return OffsetTimestamp.builder()
                .timestamp(0)
                .offset(0)
                .build();
    }
}
