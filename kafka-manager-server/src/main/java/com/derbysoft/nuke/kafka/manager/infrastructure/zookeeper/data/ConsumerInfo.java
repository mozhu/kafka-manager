package com.derbysoft.nuke.kafka.manager.infrastructure.zookeeper.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsumerInfo {

    private String consumerGroup;
    private String consumerId;

    private Map<String, Integer> subscription;
    private String pattern;

    private String version;
    private String timestamp;


}
