package com.derbysoft.nuke.kafka.manager.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Broker {
    private String id;
    private String host;
    private String port;
    private String jmxPort;
    private List<String> endpoints;

    public String getBootstrapServer() {
        return this.host + ":" + this.port;
    }

}
