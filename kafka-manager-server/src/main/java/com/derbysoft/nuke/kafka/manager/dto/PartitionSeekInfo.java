package com.derbysoft.nuke.kafka.manager.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PartitionSeekInfo {
    private Integer partition;
    private Long seekOffset;
    private Boolean seekEnable;
}
