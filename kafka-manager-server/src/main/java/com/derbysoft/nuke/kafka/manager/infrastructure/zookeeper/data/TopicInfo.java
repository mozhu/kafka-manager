package com.derbysoft.nuke.kafka.manager.infrastructure.zookeeper.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * /brokers/topics/[topic] :
 * 存储某个topic的partitions所有分配信息
 *
 * Schema:
 * {
 *     "version": "版本编号目前固定为数字1",
 *     "partitions": {
 *         "partitionId编号": [
 *             同步副本组brokerId列表
 *         ],
 *         "partitionId编号": [
 *             同步副本组brokerId列表
 *         ],
 *         .......
 *     }
 * }
 * Example:
 * {
 * "version": 1,
 * "partitions": {
 * "2": [1, 2, 3],
 * "1": [0, 1, 2],
 * "0": [3, 0, 1],
 * }
 * }
 * </pre>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TopicInfo {
    private String topic;
    private Map<String, List<Integer>> partitions;
    private String version;
}
