package com.derbysoft.nuke.kafka.manager.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class MainController {

    @GetMapping("/echo")
    public Object echo() {
        return System.currentTimeMillis();
    }

    @GetMapping("/test/error")
    public Object error() {
        throw new RuntimeException("UnknownException");
    }
}
