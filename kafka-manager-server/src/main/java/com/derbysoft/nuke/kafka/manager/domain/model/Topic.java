package com.derbysoft.nuke.kafka.manager.domain.model;

import lombok.Data;

import java.util.List;

/**
 * Created by Albert Fu on 21/05/2018.
 */
@Data
public class Topic {
    private Cluster cluster;

    private String name;
    private boolean internal;
    private List<Node> brokers;
    private List<TopicPartition> partitions;
}
