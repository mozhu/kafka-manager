package com.derbysoft.nuke.kafka.manager.infrastructure.zookeeper.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * <pre>
 * /brokers/topics/[topic]/partitions/[0...N]  其中[0..N]表示partition索引号
 * /brokers/topics/[topic]/partitions/[partitionId]/state
 *
 * Schema:
 * {
 * "controller_epoch": 表示kafka集群中的中央控制器选举次数,
 * "leader": 表示该partition选举leader的brokerId,
 * "version": 版本编号默认为1,
 * "leader_epoch": 该partition leader选举次数,
 * "isr": [同步副本组brokerId列表]
 * }
 *
 * Example:
 * {
 * "controller_epoch": 1,
 * "leader": 3,
 * "version": 1,
 * "leader_epoch": 0,
 * "isr": [3, 0, 1]
 * }
 * </pre>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TopicPartitionState {
    private String topic;
    private String partition;
    private String controller_epoch;
    private String leader_epoch;
    private String leader;
    private String version;
    private List<String> isr;
}
