package com.derbysoft.nuke.kafka.manager.integration.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class KafkaProducerStarter {

    String bootstrapServers = "localhost:9091;localhost:9092;localhost:9093";

    public void start() {
        Map<String, Object> conf = new HashMap<>();
        conf.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        conf.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        conf.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(conf);

        int batch = 0;
        while (true) {
            System.out.println(String.format("start to send batch[%s]", ++batch));
            for (int i = 0; i < 10; i++) {
                ProducerRecord<String, String> record = new ProducerRecord<>("uat_carlson_message", null, String.format("message of batch[%s], index[%s], timestamp[%s]", batch, i++, System.currentTimeMillis()));
                kafkaProducer.send(record);
            }
            try {
                TimeUnit.MILLISECONDS.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public static void main(String[] args) {
        KafkaProducerStarter starter = new KafkaProducerStarter();
        starter.start();
    }

}
