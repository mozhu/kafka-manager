package com.derbysoft.nuke.kafka.manager.infrastructure.jmx;

import com.derbysoft.nuke.kafka.manager.domain.model.Broker;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

public class JMXServiceTest {

//    Broker broker = Broker.builder()
//            .id("1")
//            .host("localhost")
//            .port("9091")
//            .jmxPort("9997")
//            .build();

    Broker broker = Broker.builder()
            .id("2")
            .host("ip-10-110-0-28.us-west-2.compute.internal")
            .port("9092")
            .jmxPort("9999")
            .build();

    JMXService jmxService = new JMXServiceImpl();

    @Test
    public void testGetSocketServerMetrics() {
        Map<String, Object> metrics = jmxService.getSocketServerMetrics(broker);
        printlnAsJson(metrics);
    }

    @Test
    public void testGetBrokerTopicMetrics() {
        Map<String, Object> metrics = jmxService.getBrokerTopicMetrics(broker);
        printlnAsJson(metrics);
    }

    @Test
    public void testGetOSMetrics() {
        Map<String, Object> metrics = jmxService.getOSMetrics(broker);
        printlnAsJson(metrics);
    }

    private void printlnAsJson(Map<String, Object> metrics) {
        try {
            StringWriter stringWriter = new StringWriter();
            new ObjectMapper().writeValue(stringWriter, metrics);
            System.out.println(stringWriter.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
