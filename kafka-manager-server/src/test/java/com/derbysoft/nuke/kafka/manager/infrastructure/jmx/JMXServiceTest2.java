package com.derbysoft.nuke.kafka.manager.infrastructure.jmx;

import com.derbysoft.nuke.common.module.json.Json;
import org.junit.Test;

import javax.management.*;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JMXServiceTest2 {

    @Test
    public void test() {
        try {
            JMXServiceURL serviceURL = new JMXServiceURL(String.format("service:jmx:rmi:///jndi/rmi://%s:%s/jmxrmi", "localhost", "9997"));
            JMXConnector connector = JMXConnectorFactory.connect(serviceURL, null);
            MBeanServerConnection connection = connector.getMBeanServerConnection();

            ObjectName objectName = new ObjectName("kafka.server:type=BrokerTopicMetrics,name=MessagesInPerSec");
            MBeanInfo mBeanInfo = connection.getMBeanInfo(objectName);

            MBeanAttributeInfo[] attributes = mBeanInfo.getAttributes();
            List<String> readableAttributes = Stream.of(attributes).filter(MBeanAttributeInfo::isReadable).map(MBeanFeatureInfo::getName).collect(Collectors.toList());

            AttributeList attributeList = connection.getAttributes(objectName, readableAttributes.toArray(new String[0]));

            Map<String, Object> objectMap = attributeList.asList().stream().collect(Collectors.toMap(Attribute::getName, Attribute::getValue));

            System.out.println(Json.getDefault().toJsonString(objectMap));

        } catch (IOException | MalformedObjectNameException | InstanceNotFoundException | IntrospectionException | ReflectionException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test3() {
        try {
            JMXServiceURL serviceURL = new JMXServiceURL(String.format("service:jmx:rmi:///jndi/rmi://%s:%s/jmxrmi", "localhost", "9997"));
            JMXConnector connector = JMXConnectorFactory.connect(serviceURL, null);
            MBeanServerConnection connection = connector.getMBeanServerConnection();


            ObjectName objectName = new ObjectName("kafka.server:type=socket-server-metrics,networkProcessor=0");
            MBeanInfo mBeanInfo = connection.getMBeanInfo(objectName);
            MBeanAttributeInfo[] attributes = mBeanInfo.getAttributes();
            List<String> readableAttributes = Stream.of(attributes).filter(MBeanAttributeInfo::isReadable).map(MBeanFeatureInfo::getName).collect(Collectors.toList());

            AttributeList attributeList = connection.getAttributes(objectName, readableAttributes.toArray(new String[0]));

            Map<String, Object> objectMap = attributeList.asList().stream().collect(Collectors.toMap(Attribute::getName, Attribute::getValue));

            System.out.println(Json.getDefault().toJsonString(objectMap));

        } catch (IOException | MalformedObjectNameException | InstanceNotFoundException | IntrospectionException | ReflectionException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test2(){
//        Double aDouble = Double.valueOf("1.671302299105678E-90");
//        System.out.println(aDouble.doubleValue());
        System.out.println(new BigDecimal("1.671302299105678E-90").toPlainString());
    }
}
