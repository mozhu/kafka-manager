package com.derbysoft.nuke.kafka.manager.integration.kafka.admin;

import kafka.admin.ConsumerGroupCommand;
import org.junit.Test;
import scala.collection.immutable.List;

public class KafkaAdminTest {

    @Test
    public void test() {
        String[] options = new String[]{
                "--zookeeper", "localhost:2181",
                "--list"
        };
        ConsumerGroupCommand.main(options);

        ConsumerGroupCommand.ConsumerGroupCommandOptions consumerGroupCommandOptions = new ConsumerGroupCommand.ConsumerGroupCommandOptions(options);

        ConsumerGroupCommand.ZkConsumerGroupService zkConsumerGroupService = new ConsumerGroupCommand.ZkConsumerGroupService(consumerGroupCommandOptions);
        List<String> stringList1 = zkConsumerGroupService.listGroups();

        System.out.println(stringList1);
//        ConsumerGroupCommand.KafkaConsumerGroupService kafkaConsumerGroupService = new ConsumerGroupCommand.KafkaConsumerGroupService(consumerGroupCommandOptions);
//        List<String> stringList = kafkaConsumerGroupService.listGroups();
//        System.out.println(stringList);
    }
}
