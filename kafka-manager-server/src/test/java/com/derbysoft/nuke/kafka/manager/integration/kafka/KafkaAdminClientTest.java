package com.derbysoft.nuke.kafka.manager.integration.kafka;

import com.derbysoft.nuke.kafka.manager.infrastructure.kafka.KafkaAdminClient;
import com.derbysoft.nuke.kafka.manager.infrastructure.kafka.DefaultKafkaAdminClient;
import com.derbysoft.nuke.kafka.manager.infrastructure.kafka.model.Message;
import com.derbysoft.nuke.kafka.manager.infrastructure.kafka.model.OffsetTimestamp;
import org.apache.kafka.common.TopicPartition;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KafkaAdminClientTest {
    String bootstrapServers = "localhost:9092";
    KafkaAdminClient kafkaAdminClient = new DefaultKafkaAdminClient(bootstrapServers);

    @Test
    public void test() {
        long start = System.currentTimeMillis();
        Map<Integer, OffsetTimestamp> consumer_offset = kafkaAdminClient.getEndOffsetTimestamps("__consumer_offsets");
        System.out.println(consumer_offset);
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    @Test
    public void testFetchMessages1() {
        long start = System.currentTimeMillis();
        List<Message<String, String>> messages = kafkaAdminClient.fetchMessages(new TopicPartition("test", 0), 650L, 10);
        System.out.println(messages);
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    @Test
    public void testFetchMessages1_O() {
        long start = System.currentTimeMillis();
        List<Message<String, String>> messages = kafkaAdminClient.fetchMessages_o(new TopicPartition("test", 0), 650L, 10);
        System.out.println(messages);
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }


    @Test
    public void testFetchMessages2() {
        long start = System.currentTimeMillis();
        Map<TopicPartition, Long> offsets = new HashMap<>();
        offsets.put(new TopicPartition("test", 0), 650L);
        List<Message<String, String>> messages = kafkaAdminClient.fetchMessages(offsets, 10);
        System.out.println(messages);
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }
}
