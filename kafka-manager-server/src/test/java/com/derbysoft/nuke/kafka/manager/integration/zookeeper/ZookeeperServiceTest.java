package com.derbysoft.nuke.kafka.manager.integration.zookeeper;

import com.derbysoft.nuke.kafka.manager.config.WebConfiguration;
import com.derbysoft.nuke.kafka.manager.infrastructure.zookeeper.ZookeeperServiceImpl;
import com.derbysoft.nuke.kafka.manager.infrastructure.zookeeper.data.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ZookeeperServiceTest {

        String connectionInfo = "localhost:2181";
//    String connectionInfo = "54.70.202.170:2181";
    String namespace = "";
    CuratorFramework client;

    ZookeeperServiceImpl zookeeperService;

    @Before
    public void setUp() {
        client = createClient();
        client.start();
        zookeeperService = new ZookeeperServiceImpl();
        zookeeperService.setClient(client);
        zookeeperService.setObjectMapper(objectMapper());
    }

    @Test
    public void testGetController() {
        Controller controller = zookeeperService.getController();
        System.out.println(controller);
    }

    @Test
    public void testGetBrokers() {
        List<String> brokerIds = zookeeperService.getBrokerIds();
        System.out.println(brokerIds);
        for (String brokerId : brokerIds) {
            BrokerInfo broker = zookeeperService.getBroker(brokerId);
            System.out.println(broker);
        }
    }

    @Test
    public void testGetTopics() {
        List<String> topicNames = zookeeperService.getTopicNames();
        System.out.println(topicNames);
        System.out.println(topicNames.size());
        for (String topicName : topicNames) {
            TopicInfo topic = zookeeperService.getTopic(topicName);
            System.out.println(topic);
        }
    }


    @Test
    public void testGetTopicPartitions() {
        List<String> topicNames = zookeeperService.getTopicNames();
        System.out.println(topicNames);
        System.out.println(topicNames.size());
        for (String topicName : topicNames) {
            System.out.println(topicName);
            List<String> partitions = zookeeperService.getPartitions(topicName);
            for (String partition : partitions) {
                TopicPartitionState topicPartitionState = zookeeperService.getTopicPartitionState(topicName, partition);
                System.out.println(topicPartitionState);
            }
        }
    }

    @Test
    public void testGetConsumers() {
        List<String> consumerGroups = zookeeperService.getConsumerGroups();
        System.out.println(consumerGroups);
        System.out.println(consumerGroups.size());
        for (String consumerGroup : consumerGroups) {
            List<ConsumerInfo> consumerInfos = zookeeperService.getConsumers(consumerGroup);
            System.out.println(String.format("consumerGroup[%s], consumers[%d]", consumerGroup, consumerInfos.size()));
            for (ConsumerInfo consumerInfo : consumerInfos) {
                System.out.println(consumerInfo);
            }
        }
    }

    private CuratorFramework createClient() {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        return CuratorFrameworkFactory.builder()
                .connectString(connectionInfo)
                .sessionTimeoutMs(5000)
                .connectionTimeoutMs(5000)
                .retryPolicy(retryPolicy)
                .namespace(namespace)
                .build();
    }

    private ObjectMapper objectMapper() {
        return new WebConfiguration().objectMapper();
    }
}
