package com.derbysoft.nuke.kafka.manager.infrastructure.kafka;

import com.derbysoft.nuke.common.module.json.Json;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.DescribeClusterResult;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.Node;
import org.junit.Test;

import java.util.Collection;

public class KafkaClientTest {

    @Test
    public void test() throws Exception{
        KafkaClientImpl kafkaClient = new KafkaClientImpl();
        AdminClient adminClient = kafkaClient.getAdminClient("localhost:9091;localhost:9092;localhost:9093");
        DescribeClusterResult describeClusterResult = adminClient.describeCluster();

        KafkaFuture<Collection<Node>> nodes = describeClusterResult.nodes();
        System.out.println("Nodes:");
        nodes.get().forEach(node -> System.out.println(Json.getDefault().toJsonString(node)));

        System.out.println("Controller:");
        KafkaFuture<Node> controller = describeClusterResult.controller();
        System.out.println(Json.getDefault().toJsonString(controller.get()));

//        System.out.println(Json.getDefault().toJsonString(describeClusterResult.clusterId().get()));


    }
}
