package com.derbysoft.nuke.kafka.manager.integration.kafka.admin;

import kafka.admin.KafkaConsumerService;
import kafka.admin.ZKConsumerService;
import kafka.admin.PartitionAssignmentState;
import org.junit.Test;

import java.util.List;

public class ConsumerServiceTest {

    ZKConsumerService zkConsumerService = new ZKConsumerService("localhost:2181");
    KafkaConsumerService kafkaConsumerService = new KafkaConsumerService("localhost:9091");

    @Test
    public void testZKConsumerService() {
        List<String> strings = zkConsumerService.listConsumerGroups();
        System.out.println(strings);
    }

    @Test
    public void testKafkaConsumerService() {
        List<PartitionAssignmentState> partitionAssignmentStates = kafkaConsumerService.collectGroupAssignment("consumer-group-1");
        for (PartitionAssignmentState partitionAssignmentState : partitionAssignmentStates) {
            System.out.println(partitionAssignmentState);
        }
    }

}
