package com.derbysoft.nuke.kafka.manager.infrastructure.zookeeper;

import com.derbysoft.nuke.kafka.manager.domain.model.Broker;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.assertj.core.util.Lists;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

public class ZookeeperClientTest {

    @Test
    public void test() throws Exception {

//        String connectionInfo = "localhost:2181";
        String connectionInfo = "54.70.202.170:2181";
        String namespace = "";
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client =
                CuratorFrameworkFactory.builder()
                        .connectString(connectionInfo)
                        .sessionTimeoutMs(5000)
                        .connectionTimeoutMs(5000)
                        .retryPolicy(retryPolicy)
                        .namespace(namespace)
                        .build();
        client.start();

        List<String> brokerIds = getBrokerIds(client);
        System.out.println(brokerIds);

        for (String brokerId : brokerIds) {
            Broker broker = getBroker(client, brokerId);
            System.out.println(broker);
        }
    }

    private List<Broker> getBrokers(CuratorFramework client) {
        return getBrokerIds(client).stream()
                .map(brokerId -> this.getBroker(client, brokerId))
                .collect(Collectors.toList());
    }

    private Broker getBroker(CuratorFramework client, String brokerId) {
        try {
            byte[] paths = client.getData().forPath(String.format("/brokers/ids/%s", brokerId));
            ZKNodeData data = new ZKNodeData(paths);
            String jmxPort = data.getString("jmx_port");
            String host = data.getString("host");
            String port = data.getString("port");
            List<String> endpoints = data.get("endpoints");
            return Broker.builder()
                    .id(brokerId)
                    .host(host)
                    .port(port)
                    .jmxPort(jmxPort)
                    .endpoints(endpoints)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<String> getBrokerIds(CuratorFramework client) {
        try {
            return client.getChildren().forPath("/brokers/ids");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Lists.emptyList();
    }
}
