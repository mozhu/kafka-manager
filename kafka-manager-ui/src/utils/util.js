export function timeFix () {
  const time = new Date()
  const hour = time.getHours()
  return hour < 9 ? '早上好' : (hour <= 11 ? '上午好' : (hour <= 13 ? '中午好' : (hour < 20 ? '下午好' : '晚上好')))
}

export function welcome () {
  const arr = ['休息一会儿吧', '准备吃什么呢?', '要不要打一把 DOTA', '我猜你可能累了']
  const index = Math.floor((Math.random() * arr.length))
  return arr[index]
}

/**
 * 触发 window.resize
 */
export function triggerWindowResizeEvent () {
  const event = document.createEvent('HTMLEvents')
  event.initEvent('resize', true, true)
  event.eventType = 'message'
  window.dispatchEvent(event)
}

/**
 * Remove loading animate
 * @param id parent element id or class
 * @param timeout
 */
export function removeLoadingAnimate (id = '', timeout = 1500) {
  if (id === '') {
    return
  }
  setTimeout(() => {
    document.body.removeChild(document.getElementById(id))
  }, timeout)
}


/**
 * convert bytes to MB GB
 */
export function standardizeBytes (bytes) {
  let size = "";
  if(bytes < 0.1 * 1024){                            //小于0.1KB，则转化成B
    size = bytes.toFixed(2) + " B"
  }else if(bytes < 0.1 * 1024 * 1024){            //小于0.1MB，则转化成KB
    size = (bytes/1024).toFixed(2) + " KB"
  }else if(bytes < 0.1 * 1024 * 1024 * 1024){        //小于0.1GB，则转化成MB
    size = (bytes/(1024 * 1024)).toFixed(2) + " MB"
  }else{                                            //其他转化成GB
    size = (bytes/(1024 * 1024 * 1024)).toFixed(2) + " GB"
  }

  let sizeStr = size + "";                        //转成字符串
  let index = sizeStr.indexOf(".");                    //获取小数点处的索引
  let dou = sizeStr.substr(index + 1 ,2)            //获取小数点后两位的值
  if(dou == "00"){                                //判断后两位是否为00，如果是则删除00
    return sizeStr.substring(0, index) + sizeStr.substr(index + 3, 2)
  }
  return size;
}