import {axios} from '@/utils/request'
import Qs from 'qs'

const http = {}

http.get = function ({url}) {
  return axios({
    method: 'get',
    url
  });
}

http.post = function ({url, data}) {
  return axios({
    method: 'post',
    url,
    data
  });
}

http.postForm = function ({url, data}) {
  return axios({
    method: 'post',
    url,
    data,
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    transformRequest: [function (data) {
      return Qs.stringify(data, { indices: false });
    }],
  });
}

http.postWithPagination = function ({url, pagination, sort, data}) {
  let paginationData = {...data}
  if (pagination) {
    paginationData.page = pagination.currentPage - 1 // 后端页数从0开始
    paginationData.size = pagination.pageSize
  }
  if (sort && sort.order && sort.prop) {
    paginationData.sort = sort.prop + ',' + (sort.order === 'ascending' ? 'asc' : 'desc')
  }
  return http.postForm({
    url,
    data: paginationData
  })
}

export default http
