import {axios} from '@/utils/request'
import http from '@/utils/http'

const baseUrl = '';

const listClusters = () => http.get({
  url: `${baseUrl}/clusters`
})

const saveCluster = (cluster) => http.post({
  url: `${baseUrl}/cluster/save`,
  data: cluster
})

const deleteCluster = (clusterCode) => http.get({
  url: `${baseUrl}/cluster/${clusterCode}/delete`
})

const batchDeleteClusters = (codes) => http.postForm({
  url: `${baseUrl}/cluster/batch/delete`,
  data: codes
})

const listClusterTopics = (clusterCode) => http.get({
  url: `${baseUrl}/cluster/${clusterCode}/topics`
})

const listClusterBrokers = (clusterCode) => http.get({
  url: `${baseUrl}/cluster/${clusterCode}/brokers`
})

const listClusterBrokersMetrics = (clusterCode) => http.get({
  url: `${baseUrl}/cluster/${clusterCode}/brokers/metrics`
})

const getClusterTopic = (clusterCode, topicName) => http.get({
  url: `${baseUrl}/cluster/${clusterCode}/topic/${topicName}`
})

const listTopicPartitionSummaries = (clusterCode, topicName) => http.get({
  url: `${baseUrl}/cluster/${clusterCode}/topic/${topicName}/partition/summaries`
})

const seekTopicMessages = (clusterCode, topicName, topicMessageSeekSeq) => http.post({
  url: `${baseUrl}/cluster/${clusterCode}/topic/${topicName}/message/seek`,
  data: topicMessageSeekSeq
})

export default {
  listClusters,
  saveCluster,
  deleteCluster,
  batchDeleteClusters,
  listClusterTopics,
  listClusterBrokers,
  listClusterBrokersMetrics,
  getClusterTopic,
  listTopicPartitionSummaries,
  seekTopicMessages
}